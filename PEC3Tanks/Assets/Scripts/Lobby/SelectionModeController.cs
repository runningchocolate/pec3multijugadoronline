﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectionModeController : MonoBehaviour
{
    public string scene1v1;
    public string sceneLAN;
    public string sceneWAN;

    public void Mode1v1()
    {
        SceneManager.LoadScene(scene1v1);
    }

    public void ModeTeam()
    {
       SceneManager.LoadScene(sceneLAN);
    }

    public void ModeFFA()
    {
        SceneManager.LoadScene(sceneWAN);
    }
}