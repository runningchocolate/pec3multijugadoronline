using UnityEngine;
using System.Collections;

public class BulletOnline : MonoBehaviour
{
    [HideInInspector] public int playerId;

    void OnCollisionEnter(Collision collision) {
		
        GameObject hit = collision.gameObject;

        //Health health = hit.GetComponent<Health>();

        //Depen de com es digui la classe finalment... faig exemple amb TankHealthOffline pero s'ha de cambiar

        TankHealthOffline health = hit.GetComponent<TankHealthOffline>();
        if (health != null)
        {
            health.TakeDamage(10);
        }

        Destroy(gameObject);       
    }
}
