﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TurretManager : NetworkBehaviour {

    public GameObject turretPrefab;
    public Transform wayPoint;

    public override void OnStartServer()
    {
        GameObject turret = (GameObject)Instantiate(turretPrefab, wayPoint.position, wayPoint.rotation);
        NetworkServer.Spawn(turret);
    }
}
