﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TurretOnline : NetworkBehaviour {

    float timer;
    float delay;
    int playersLayer;

    public GameObject bulletPrefab;                                   // Prefab of the shell.
    public Transform bulletSpawn;

    void Start () {
        timer = 0f;
        delay = 1f;
        playersLayer = LayerMask.NameToLayer("Players");
    }
	
	void Update () {
        timer += Time.deltaTime;
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == playersLayer)
        {
            transform.LookAt(other.transform);
            if (timer >= delay)
            {
                CmdFire();
                timer = 0f;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == playersLayer)
        {
            transform.LookAt(other.transform);
            if (timer >= delay)
            {
                CmdFire();
                timer = 0f;
            }
        }
    }

    [Command]
    private void CmdFire()
    {
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 10;
        NetworkServer.Spawn(bullet);

        Destroy(bullet, 2.0f);
    }
}
