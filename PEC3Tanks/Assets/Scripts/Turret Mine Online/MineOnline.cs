﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MineOnline : NetworkBehaviour {

    int playersLayer;

    private void Start()
    {
        playersLayer = LayerMask.NameToLayer("Players");
    }

    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.layer == playersLayer)
        {
            CmdExplode(collision.gameObject);           
        }
    }

    [Command]
    private void CmdExplode(GameObject collision)
    {
        GameObject hit = collision.gameObject;
        //Health health = hit.GetComponent<Health>();

        //Depen de com es digui la classe finalment... faig exemple amb TankHealthOffline pero s'ha de cambiar

        TankHealthOffline health = hit.GetComponent<TankHealthOffline>(); 

        if (health != null)
        {
            health.TakeDamage(60);
        }
        Destroy(gameObject);
    }
}
