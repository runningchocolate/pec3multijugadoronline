﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MineSpawner : NetworkBehaviour{

    public GameObject minePrefab;
    public Transform wayPoint;

    public override void OnStartServer()
    {
        GameObject mine = (GameObject)Instantiate(minePrefab, wayPoint.position, wayPoint.rotation);
        NetworkServer.Spawn(mine);
    }
}
