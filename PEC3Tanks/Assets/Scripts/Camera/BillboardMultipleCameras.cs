using UnityEngine;
using System.Collections;

public class BillboardMultipleCameras : MonoBehaviour
{
    private Camera cam = null;

    private Camera mainCam = null;

	// Update is called once per frame
	void Update () {
	    if (cam == null)
	    {
	        cam = GetComponentInParent<Transform>().GetComponentInChildren<Camera>();
	        
	    }
	    else
	    {
	        if (cam.isActiveAndEnabled)
	        {
	            transform.LookAt(cam.transform);
            }
	        else
	        {
	            if (mainCam == null)
	            {
	                mainCam = GameObject.Find("Main Camera").GetComponent<Camera>();
	            }
                transform.LookAt(mainCam.transform);
	        }
        }

	    
    }
}
