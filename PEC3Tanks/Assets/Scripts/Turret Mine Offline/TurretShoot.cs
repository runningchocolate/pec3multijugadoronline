﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TurretShoot : MonoBehaviour {
    //OFFLINE
    
    public float m_MinLaunchForce = 15f;        
    public float m_MaxLaunchForce = 30f;       
    public float m_MaxChargeTime = 0.75f;      

    private float m_CurrentLaunchForce;       

    float timer;
    float delay;
    int playersLayer;
    int ignoreLayer;

    public Transform fireTransform;
    public Rigidbody m_Shell;                                               // Prefab of the shell.

    void Start () {
        timer = 0f;
        delay = 1f;
        playersLayer = LayerMask.NameToLayer("Players");
        ignoreLayer = LayerMask.NameToLayer("WeaponUI");
        m_CurrentLaunchForce = m_MinLaunchForce;
    }
	
	void Update () {
        timer += Time.deltaTime;
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == playersLayer)                        // Layer players
        {
            transform.LookAt(other.transform);
            if (timer >= delay)
            {
                Shoot();
                timer = 0f;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == playersLayer)
        {
            transform.LookAt(other.transform);
            if (timer >= delay)
            {
                Shoot();
                timer = 0f;
            }
        }
    }

    public void Shoot()
    {
        Physics.IgnoreLayerCollision(0, ignoreLayer);
        float speed = 1.5f;
        Rigidbody shellInstance = Instantiate(m_Shell, fireTransform.position, fireTransform.rotation) as Rigidbody;
        shellInstance.velocity = m_CurrentLaunchForce * fireTransform.forward * speed;
        m_CurrentLaunchForce = m_MinLaunchForce;
    }
}
