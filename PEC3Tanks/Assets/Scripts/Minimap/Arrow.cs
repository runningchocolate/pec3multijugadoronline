﻿using UnityEngine;


    public class Arrow
    {
        public Vector2 position;
        public Vector3 rotation;
        public GameObject arrow;
        public Color color;
        public int player;
    }

