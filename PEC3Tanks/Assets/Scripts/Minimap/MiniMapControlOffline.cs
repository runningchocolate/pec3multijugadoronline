﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMapControlOffline : MonoBehaviour
{
    public Dictionary<int, Arrow> arrows;
    public GameObject arrow;

    public GameManagerOffline gmo;

    private int initialRotation = 230;

    private float realWorldXmin = -55f;
    private float realWorldXmax = 55f;
    private float realWorldZmin = -55f;
    private float realWorldZmax = 55f;

    private float anchoredPositionX = -120f;
    private float anchoredPositionY = -120f;

    private Boolean isCenter = true;

    void Start()
    {
        SetPlayers();
        gmo = GameObject.Find("GameManager").GetComponent<GameManagerOffline>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf)
        {
            List<TankManagerOffline> tanks = gmo.m_Tanks;
            if(arrows==null) SetPlayers();
            int pos = 0;
            foreach (TankManagerOffline tank in tanks)
            {
                if (tank.m_Instance != null)
                {
                    pos = tank.m_PlayerNumber -1;
                    Vector3 position = GetPosition(tank.m_Instance.transform.position);
                    Vector3 rotation = GetRotation(tank.m_Instance.transform.eulerAngles);
                    if (arrows.ContainsKey(pos))
                    {
                        arrows[pos].position = position;
                        arrows[pos].rotation = rotation;
                        arrows[pos].arrow.transform.localPosition = position;
                        arrows[pos].arrow.transform.eulerAngles = rotation;
                    }
                    else
                    {
                        GameObject arr = Instantiate(arrow, new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0)) as GameObject;

                        arr.GetComponent<RawImage>().color = new Color(tank.m_PlayerColor.r, tank.m_PlayerColor.g, tank.m_PlayerColor.b, 255); ;
                        arrows.Add(pos, new Arrow());
                        arrows[pos].arrow = arr;
                        arrows[pos].color = tank.m_PlayerColor;
                        arrows[pos].player = pos;
                        arrows[pos].position = position;
                        arrows[pos].arrow.transform.parent = transform;
                        arrows[pos].arrow.transform.localPosition = position;
                        arrows[pos].rotation = rotation;
                        arrows[pos].arrow.transform.eulerAngles = rotation;
                    }
                }                 
            }

            if (isCenter != GameObject.Find("CameraRig").GetComponent<CameraControlOffline>().IsSplitScreen())
            {
                isCenter = GameObject.Find("CameraRig").GetComponent<CameraControlOffline>().IsSplitScreen();
                if (isCenter) SetCenter();
                else SetTopRight();
            }
        }
    }

    public void SetPlayers()
    {
        if(arrows == null)
            arrows = new Dictionary<int, Arrow>();
        RespawnPlayers();
    }

    private Vector3 GetPosition(Vector3 tankPosition)
    {
        float absoluteXWorld = Mathf.Abs(realWorldXmin) + Mathf.Abs(realWorldXmax);
        float absolutePositionX = tankPosition.x + absoluteXWorld / 2;
        float xPositionRelative = absolutePositionX / absoluteXWorld;
        float xFinalPosition = (GetComponent<RectTransform>().rect.width /2) - GetComponent<RectTransform>().rect.width * xPositionRelative;

        float absoluteZWorld = Mathf.Abs(realWorldZmin) + Mathf.Abs(realWorldZmax);
        float absolutePositionZ = tankPosition.z + absoluteZWorld / 2;
        float zPositionRelative = absolutePositionZ / absoluteZWorld;
        float zFinalPosition = (GetComponent<RectTransform>().rect.height / 2) - GetComponent<RectTransform>().rect.height * zPositionRelative;

        return new Vector3(xFinalPosition, zFinalPosition ,0f);
    }

    private Vector3 GetRotation(Vector3 tankRotation)
    {
        return new Vector3(0f, 0f, -tankRotation.y + initialRotation);
    }

    private void SetTopRight()
    {
        GetComponent<RectTransform>().anchorMin = new Vector2(1f, 1f);
        GetComponent<RectTransform>().anchorMax = new Vector2(1f, 1f);
        GetComponent<RectTransform>().anchoredPosition = new Vector3(anchoredPositionX, anchoredPositionY, 0f);
    }

    private void SetCenter()
    {
        GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
        GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
        GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0f);
    }

    public void RespawnPlayers()
    {
        foreach (var a in arrows)
        {
            a.Value.arrow.SetActive(true);
        }
    }

    public void RemovePlayer(int i)
    {
        foreach (var a in arrows)
        {
            if(a.Value.player==i) a.Value.arrow.SetActive(false);
        }
    }
}
