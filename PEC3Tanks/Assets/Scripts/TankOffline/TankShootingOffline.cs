﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TankShootingOffline : MonoBehaviour
{
    public int m_PlayerNumber = 1;              // Used to identify the different players.
    public Rigidbody m_Shell;                   // Prefab of the shell.
    public Rigidbody m_ShellRed;                   // Prefab of the shell.
    public Transform m_FireTransform;           // A child of the tank where the shells are spawned.
    public Slider m_AimSlider;                  // A child of the tank that displays the current launch force.
    public AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
    public AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up.
    public AudioClip m_FireClip;                // Audio that plays when each shot is fired.
    public float m_MinLaunchForce = 15f;        // The force given to the shell if the fire button is not held.
    public float m_MaxLaunchForce = 30f;        // The force given to the shell if the fire button is held for the max charge time.
    public float m_MaxChargeTime = 0.75f;       // How long the shell can charge for before it is fired at max force.
    [HideInInspector]
    public int m_localID;

    private string m_FireButton;                // The input axis that is used for launching shells.
    private string m_ReloadButton;                // The input axis that is used for launching shells.
    private string m_AltFireButton;                // The input axis that is used for launching shells.
    private float m_CurrentLaunchForce;         // The force that will be given to the shell when the fire button is released.
    private float m_ChargeSpeed;                // How fast the launch force increases, based on the max charge time.
    private bool m_Fired;                       // Whether or not the shell has been launched with this button press.
    private bool m_AltFired;                       // Whether or not the shell has been launched with this button press.


    public GameObject bulletCanvas;
    public const int maxBullets = 5;
    public int reloadTime = 2; //seconds
    private float startReloadTime = -1;
    public GameObject reloadBar;
    public int currentBullets = maxBullets;

    private void OnEnable()
    {
        // When the tank is turned on, reset the launch force and the UI
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_AimSlider.value = m_MinLaunchForce;
    }


    private void Start()
    {
        // The fire axis is based on the player number.
        m_FireButton = "Fire" + m_PlayerNumber;
        m_AltFireButton = "AltFire" + m_PlayerNumber;
        m_ReloadButton = "Reload" + m_PlayerNumber;

        // The rate that the launch force charges up is the range of possible forces by the max charge time.
        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
    }


    private void Update()
    {
        if (currentBullets <= 0 && startReloadTime == -1)
        {
            Reload();
        }
        else if (Input.GetButton(m_ReloadButton) && startReloadTime == -1 && currentBullets < maxBullets)
        {
            Reload();
        }
        

        if (startReloadTime != -1)
        {
            float elapsedTime = Time.time - startReloadTime;
            if (elapsedTime < reloadTime)
            {
                float percentage = elapsedTime / ((float)reloadTime) * 100;
                GameObject fg = reloadBar.transform.Find("Foreground").gameObject;
                fg.GetComponent<RectTransform>().sizeDelta = new Vector2(percentage, 10f);
            }
            else
            {
                //reloadBar.SetActive(false);
                ReloadBullets();
                startReloadTime = -1;
                reloadBar.transform.Find("Foreground").GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 10f);
            }
        }
        else if(currentBullets > 0)
        {
            
                MakeFire();
            
        }
    }

    private void ReloadBullets()
    {
        currentBullets = maxBullets;
        ChangeBullets(currentBullets);
    }

    private void Reload()
    {       
        startReloadTime = Time.time;
        //reloadBar.SetActive(true);
        reloadBar.transform.Find("Foreground").GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 10f);        
    }


    private void MakeFire()
    {
        // The slider should have a default value of the minimum launch force.
        m_AimSlider.value = m_MinLaunchForce;
        //Debug.Log(Input.GetButtonDown(m_AltFireButton) + " " + Input.GetButton(m_AltFireButton) + " " + Input.GetButtonUp(m_AltFireButton));
        // If the max force has been exceeded and the shell hasn't yet been launched...
        if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
        {
            // ... use the max force and launch the shell.
            m_CurrentLaunchForce = m_MaxLaunchForce;
            Fire(false);
        }
        else if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_AltFired)
        {
            // ... use the max force and launch the shell.
            // Debug.Log("Fire by max force");
            m_CurrentLaunchForce = m_MaxLaunchForce;
            Fire(true);
        }
        // Otherwise, if the fire button has just started being pressed...
        else if (Input.GetButtonDown(m_FireButton))
        {
            // ... reset the fired flag and reset the launch force.
            m_Fired = false;
            m_AltFired = true;
            m_CurrentLaunchForce = m_MinLaunchForce;

            // Change the clip to the charging clip and start it playing.
            m_ShootingAudio.clip = m_ChargingClip;
            m_ShootingAudio.Play();
        }
        else if (Input.GetButtonDown(m_AltFireButton))
        {
            //Debug.Log("Start Charge");
            // ... reset the fired flag and reset the launch force.
            m_AltFired = false;
            m_Fired = true;
            m_CurrentLaunchForce = m_MinLaunchForce;

            // Change the clip to the charging clip and start it playing.
            m_ShootingAudio.clip = m_ChargingClip;
            m_ShootingAudio.Play();
        }
        // Otherwise, if the fire button is being held and the shell hasn't been launched yet...
        else if ((Input.GetButton(m_FireButton) && !m_Fired)
                 || (Input.GetButton(m_AltFireButton) && !m_AltFired))
        {
            //Debug.Log("Charging");
            // Increment the launch force and update the slider.
            m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;

            m_AimSlider.value = m_CurrentLaunchForce;
        }
        // Otherwise, if the fire button is released and the shell hasn't been launched yet...
        else if (Input.GetButtonUp(m_FireButton) && !m_Fired)
        {
            // ... launch the shell.
            m_Fired = true;
            Fire(false);
        }
        else if (Input.GetButtonUp(m_AltFireButton) && !m_AltFired)
        {
            //Debug.Log("Fire by release");
            // ... launch the shell.
            m_AltFired = true;
            Fire(true);
        }
    }


    private void Fire(Boolean fast)
    {
        // Set the fired flag so only Fire is only called once.


        float speed = fast ? 1.0f : 1.5f;

        // Create an instance of the shell and store a reference to it's rigidbody.
        Rigidbody shellInstance = fast ?
            Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody :
            Instantiate(m_ShellRed, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

        // Set the shell's velocity to the launch force in the fire position's forward direction.
        shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward * speed;

        // Change the clip to the firing clip and play it.
        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play();

        // Reset the launch force.  This is a precaution in case of missing button events.
        m_CurrentLaunchForce = m_MinLaunchForce;
        currentBullets--;
        ChangeBullets(currentBullets);
    }

    public void SetDefaults()
    {
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_AimSlider.value = m_MinLaunchForce;
    }

    private void ChangeBullets(int currentBullet)
    {
        //Debug.Log("Current bullet: " + currentBullet + " " + gameObject.name);
        int i = 0;
        currentBullets = currentBullet;
        Transform[] bulletList = bulletCanvas.transform.GetComponentsInChildren<Transform>();
        foreach (Transform bullet in bulletList)
        {
            if (bullet.gameObject.GetComponent<RawImage>() != null)
            {
                if (i >= currentBullet)
                {
                    RawImage rawImage = bullet.gameObject.GetComponent<RawImage>();
                    Color currColor = rawImage.color;
                    currColor.a = 0.5f;
                    rawImage.color = currColor;
                }
                else
                {
                    RawImage rawImage = bullet.gameObject.GetComponent<RawImage>();
                    Color currColor = rawImage.color;
                    currColor.a = 1f; rawImage.color = currColor;
                    rawImage.color = currColor;
                }
                i++;
            }
        }
    }
}
