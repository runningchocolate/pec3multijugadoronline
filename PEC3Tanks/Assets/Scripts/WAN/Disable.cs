﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disable : MonoBehaviour {
    
	void Start () {
        GameObject.FindGameObjectWithTag("FFALobby").GetComponent<Canvas>().enabled = false;
	}
}
