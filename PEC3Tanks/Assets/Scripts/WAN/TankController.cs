using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class TankController : NetworkBehaviour
{
    public static int currentPlayers = 0;
    public int currentPlayer;

    [SyncVar]
    public string nickname;

    [SyncVar]
    public Color colour;

    //bullet system
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public GameObject bulletCanvas;
    public const int maxBullets = 5;
    public int reloadTime = 2; //seconds
    private float startReloadTime = -1;
    private GameObject reloadBar;

    [SyncVar(hook = "OnChangeBullets")]
    public int currentBullets = maxBullets;


    //statistics params
    private bool automaticMovement = false;

    void Start()
    {
        currentBullets = maxBullets;
        reloadBar = GameObject.Find("ReloadBar");
        //reloadBar.SetActive(false);
        currentPlayer = ++TankController.currentPlayers;

        nickname = "Player" + currentPlayers.ToString();
        CmdChangeName(nickname);
    }

    void OnGUI()
    {
        if (isLocalPlayer)
        {
            nickname = GUI.TextField(new Rect(25, Screen.height - 40, 100, 30), nickname);
            if (GUI.Button(new Rect(130, Screen.height - 40, 80, 30), "Change"))
            {
                CmdChangeName(nickname);
            }

            if (GUI.Button(new Rect(250, Screen.height - 40, 80, 30), "Blue"))
            {
                CmdChangeColor(Color.blue);
            }
            
            if (GUI.Button(new Rect(350, Screen.height - 40, 80, 30), "Yellow"))
            {
                CmdChangeColor(Color.yellow);
            }

            if (GUI.Button(new Rect(450, Screen.height - 40, 80, 30), "Green"))
            {
                CmdChangeColor(Color.green);
            }

            if (GUI.Button(new Rect(550, Screen.height - 40, 80, 30), "Magenta"))
            {
                CmdChangeColor(Color.magenta);
            }

            if (GUI.Button(new Rect(650, Screen.height - 40, 80, 30), "Black"))
            {
                CmdChangeColor(Color.black);
            }

            if (GUI.Button(new Rect(750, Screen.height - 40, 80, 30), "Grey"))
            {
                CmdChangeColor(Color.grey);
            }

            if (GUI.Button(new Rect(850, Screen.height - 40, 80, 30), "White"))
            {
                CmdChangeColor(Color.white);
            }
        }
    }


    private void UpdateNicknameColor()
    {
        this.GetComponentInChildren<TextMesh>().text = nickname;

        foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>())
        {
            child.material.color = colour;
        }
    }

    private void UpdateMovement()
    {
        float x = 0;
        float z = 0;

        if (automaticMovement)
        {
            x = 0.5f * Time.deltaTime * 150.0f;
            z = 1f * Time.deltaTime * 3.0f;
        }
        else
        {
            x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
            z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
        }


        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
    }

    private void UpdateShootReload()
    {
        if (Input.GetKeyDown(KeyCode.Space) && currentBullets > 0 && startReloadTime == -1)
        {
            CmdFire();
        }
        else if (Input.GetKeyDown(KeyCode.Space) && currentBullets <= 0 && startReloadTime == -1)
        {
            Reload();
        }
        else if (Input.GetKeyDown(KeyCode.R) && startReloadTime == -1 && currentBullets < maxBullets)
        {
            Reload();
        }

        if (startReloadTime != -1)
        {
            float elapsedTime = Time.time - startReloadTime;
            if (elapsedTime < reloadTime)
            {
                float percentage = elapsedTime / ((float)reloadTime) * 100;
                GameObject fg = reloadBar.transform.Find("Foreground").gameObject;
                fg.GetComponent<RectTransform>().sizeDelta = new Vector2(percentage, 10f);
            }
            else
            {
                //reloadBar.SetActive(false);
                CmdReloadBullets();
                startReloadTime = -1;
                reloadBar.transform.Find("Foreground").GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 10f);
            }
        }
    }


    void Update() {
        UpdateNicknameColor();

        if (!isLocalPlayer)
        {
            return;
        }

        UpdateMovement();
        UpdateShootReload();
    }


    //Commands


    // This [Command] code is called on the Client …
    // … but it is run on the Server!
    [Command]
    void CmdFire()
    {       
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletSpawn.position,
            bulletSpawn.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 10;

        // Spawn the bullet on the Clients
        NetworkServer.Spawn(bullet);

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
        currentBullets--;
    }

    [Command]
    public void CmdChangeName(string name)
    {
        nickname = name;
    }

    [Command]
    public void CmdChangeColor(Color color)
    {
        colour = color;
    }

    [Command]
    void CmdReloadBullets()
    {
        currentBullets = maxBullets;
    }

    private void Reload()
    {
        if (isLocalPlayer)
        {
            startReloadTime = Time.time;
            //reloadBar.SetActive(true);
            reloadBar.transform.Find("Foreground").GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 10f);
        }
    }

    private void OnChangeBullets(int currentBullet)
    {
        //Debug.Log("Current bullet: " + currentBullet + " " + gameObject.name);
        int i = 0;
        currentBullets = currentBullet;
        Transform[] bulletList = bulletCanvas.transform.GetComponentsInChildren<Transform>();
        foreach (Transform bullet in bulletList)
        {
            if (bullet.gameObject.GetComponent<RawImage>() != null)
            {
                if (i >= currentBullet)
                {
                    RawImage rawImage = bullet.gameObject.GetComponent<RawImage>();
                    Color currColor = rawImage.color;
                    currColor.a = 0.5f;
                    rawImage.color = currColor;
                }
                else
                {
                    RawImage rawImage = bullet.gameObject.GetComponent<RawImage>();
                    Color currColor = rawImage.color;
                    currColor.a = 1f; rawImage.color = currColor;
                    rawImage.color = currColor;
                }
                i++;
            }            
        }
    }

    public override void OnStartLocalPlayer() {

        foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>())
        {
            child.material.color = Color.blue;
        }

        colour = Color.blue;      
        CmdChangeColor(Color.blue);
    }
}
