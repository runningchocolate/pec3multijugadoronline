using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManagerOffline : MonoBehaviour
{
    public int m_NumRoundsToWin = 5;                    // The number of rounds a single player has to win to win the game.
    public float m_StartDelay = 3f;                     // The delay between the start of RoundStarting and RoundPlaying phases.
    public float m_EndDelay = 3f;                       // The delay between the end of RoundPlaying and RoundEnding phases.
    public float m_ChangePlayerDelay = 7f;
    public CameraControlOffline m_CameraControl;       // Reference to the CameraControl script for control during different phases.
    public Text m_MessageText;                          // Reference to the overlay Text to display winning text, etc.
    public GameObject m_TankPrefab;                     // Reference to the prefab the players will control.
    public List<TankManagerOffline> m_Tanks;               // A collection of managers for enabling and disabling different aspects of the tanks.
    public GameObject followCam;
    public GameObject miniMap;
    //public GameObject addPlayer3Button;
    //public GameObject addPlayer4Button;

    private int m_RoundNumber;                          // Which round the game is currently on.
    private WaitForSeconds m_StartWait;                 // Used to have a delay whilst the round starts.
    private WaitForSeconds m_EndWait;                   // Used to have a delay whilst the round or game ends.
    private WaitForSeconds m_ChangePlayerWait;
    private TankManagerOffline m_RoundWinner;           // Reference to the winner of the current round.  Used to make an announcement of who won.
    private TankManagerOffline m_GameWinner;            // Reference to the winner of the game.  Used to make an announcement of who won.
    private Dictionary<int, DisplaySection> m_cameras;
        
    private int currentTanks = 0;
    private int currentCams = 0;
    public int maxTanks = 2;
    private int currentFollowCam = 0;

    private float initialCameraXposition = -21.8f;
    private float initialCameraYposition = 29.4f;
    private float initialCameraZposition = -18.3f;

    private float cameraScaleValue = 2.4f;

    private float rotationCameraX = 40f;
    private float rotationCameraY = 60f;
    private float rotationCameraZ = 0f;

    private int twoPlayerScreenViewport = 5;
    private int twoPlayersCamera = 2;
    private int threePlayersCamera = 3;
    private int lastPositionCamera = 4;

    public static int theoricTankNumber = 2;

    private void Start()
    {
        m_cameras = new Dictionary<int, DisplaySection>();
        // Create the delays so they only have to be made once.
        m_StartWait = new WaitForSeconds (m_StartDelay);
        m_EndWait = new WaitForSeconds (m_EndDelay);
        m_ChangePlayerWait = new WaitForSeconds(m_ChangePlayerDelay);

        miniMap.SetActive(false);

        SpawnAllTanks();
        SetCameraTargets();

        // Once the tanks have been created and the camera is using them as targets, start the game.
        StartCoroutine (GameLoop ());

        //followCam = GameObject.Find("DynCamera");
        //if (currentTanks >= maxTanks-1) addPlayer3Button.SetActive(false);
        //if (currentTanks >= maxTanks) addPlayer4Button.SetActive(false);
    }

    public void AddPlayer(int i)
    {
        if (currentTanks < maxTanks)
        {
            CreateTank(i);
            GameManagerOffline.theoricTankNumber++;
            SetCameraTargets();
        }
        //if (i == 3) addPlayer3Button.SetActive(false);
        //if (i == 4) addPlayer4Button.SetActive(false);
    }
    private void SpawnAllTanks()
    {
        currentTanks = 0;
        // For all the tanks...
        for (int i = 0; i < GameManagerOffline.theoricTankNumber; i++)
        {
            
            CreateTank();
        }

        RefreshCams();
        DesactiveMainCam();
    }

    private void CreateTank()
    {
        m_Tanks[currentTanks].m_Instance =
            Instantiate(m_TankPrefab, m_Tanks[currentTanks].m_SpawnPoint.position, m_Tanks[currentTanks].m_SpawnPoint.rotation) as GameObject;
        m_Tanks[currentTanks].m_PlayerNumber = currentTanks + 1;
        m_Tanks[currentTanks].Setup();
        AddCamera(currentTanks);
            
        currentTanks++;
    }

    private void CreateTank(int i)
    {
        i = i - 1; //p.e. player 1 ser� el 0 internamente
        m_Tanks[i].m_Instance =
            Instantiate(m_TankPrefab, m_Tanks[i].m_SpawnPoint.position, m_Tanks[i].m_SpawnPoint.rotation) as GameObject;
        m_Tanks[i].m_PlayerNumber = i + 1;
        m_Tanks[i].Setup();
        AddCamera(i);

        currentTanks++;
    }

    private void DesactiveMainCam()
    {
        Camera mainCam = GameObject.Find("Main Camera").GetComponent<Camera>();
        mainCam.gameObject.SetActive(false);
    }

    private void AddCamera(int i)
    {
        GameObject childCam = new GameObject("Camera" + (i+1));
        Camera newCam = childCam.AddComponent<Camera>();
        //Debug.Log(mainCam.transform);
        //newCam.CopyFrom(mainCam);
            
        newCam.transform.localPosition = new Vector3(initialCameraXposition, initialCameraYposition, initialCameraZposition);
        //newCam.transform.localPosition = new Vector3(0f, 0f, 0f);
            
        newCam.transform.localScale = new Vector3(cameraScaleValue, cameraScaleValue, cameraScaleValue);

        
        newCam.transform.eulerAngles = new Vector3(rotationCameraX, rotationCameraY, rotationCameraZ);
        //newCam.transform.rotation = new Quaternion(0.3f, 0.5f, -0.2f, 0.8f);
        //newCam.transform.up = new Vector3(0.6f, 0.8f, 0.3f);
        newCam.orthographic = true;
        newCam.orthographicSize = 14;
        newCam.backgroundColor = Functions.HexToColor("6B4B2105");

        childCam.transform.parent = m_Tanks[i].m_Instance.transform;

        m_cameras.Add(i, new DisplaySection(newCam, i, true));
        if (m_cameras.Count < twoPlayersCamera)  newCam.rect = GetViewport(i+twoPlayerScreenViewport+1); 
        else  m_cameras[i].m_Camera.rect = GetViewport(i+1);


        if (!GameObject.Find("CameraRig").GetComponent<CameraControlOffline>().IsSplitScreen())
        {
            m_cameras[i].m_Camera.enabled = false;
        }

        RefreshCams();
    }

    private void RefreshCams()
    {
        if (ActiveCams() <= 1)
        {
            foreach (KeyValuePair<int, DisplaySection> cam in m_cameras)
            {
                if (cam.Value.hasPlayer) { cam.Value.m_Camera.rect = GetViewport(0); }
            }
        }
        else if (ActiveCams() <= twoPlayersCamera)
        {
            int i = 0;
            followCam.gameObject.SetActive(false);
            foreach (KeyValuePair<int, DisplaySection> cam in m_cameras)
            {
                if (cam.Value.hasPlayer)
                {
                    cam.Value.m_Camera.rect = GetViewport(i + twoPlayerScreenViewport);
                    i++;
                }
                    
                    
            }
        }
        else if (ActiveCams() == threePlayersCamera)
        {
            if (m_cameras.Count < maxTanks)
            {
                int i = 0;
                Boolean found = false;
                foreach (KeyValuePair<int, DisplaySection> cam in m_cameras)
                {
                    cam.Value.m_Camera.gameObject.SetActive(true);
                    cam.Value.m_Camera.rect = GetViewport(cam.Value.position+1);
                    if (cam.Value.position != i && !found)
                    {
                        found = true;
                    }
                    else if(!found)
                    {
                        i++;
                    }

                }

                followCam.SetActive(true);
                if(found)
                    followCam.GetComponentInChildren<Camera>().rect = GetViewport(i + 1);
                else
                    followCam.GetComponentInChildren<Camera>().rect = GetViewport(lastPositionCamera);
                StartCoroutine(ChangePlayerView());
                    
            }
            else
            {
                foreach (KeyValuePair<int, DisplaySection> cam in m_cameras)
                {
                    cam.Value.m_Camera.gameObject.SetActive(true);
                    cam.Value.m_Camera.rect = GetViewport(cam.Value.position + 1);
                }
                Boolean found = false;
                for (int i = 0; i < m_cameras.Count; i++)
                {
                    if (!m_cameras[i].hasPlayer)
                    {
                        followCam.SetActive(true);
                        followCam.GetComponentInChildren<Camera>().rect = GetViewport(i+1);
                        StartCoroutine(ChangePlayerView());
                        found = true;
                    }
                }

                if (!found)
                {
                    followCam.SetActive(true);
                    followCam.GetComponentInChildren<Camera>().rect = GetViewport(lastPositionCamera);
                    StartCoroutine(ChangePlayerView());
                }
            }
        }
        else if (ActiveCams() == maxTanks)
        {
            int i = 0;
            followCam.gameObject.SetActive(false);
            foreach (KeyValuePair<int, DisplaySection> cam in m_cameras)
            {
                cam.Value.m_Camera.rect = GetViewport(i+1);
                i++;
            }
        }
    }

    public Rect GetViewport(int i)
    {
        float ra = (float)Screen.height / (float)Screen.width;
        switch (i)
        {
            case 0:
                return new Rect(0.0f, 0.0f, 1f, 1f);
            case 1:
                return new Rect(0.0f, 0.5f, 0.5f, 0.5f);
            case 2:
                return new Rect(0.5f, 0.5f, 0.5f, 0.5f);
            case 3:
                return new Rect(0.0f, 0.0f, 0.5f, 0.5f);
            case 4:
                return new Rect(0.5f, 0.0f, 0.5f, 0.5f);
            case 5:
                //A.R. 16:9   -   0.5 / 9 X 16 = 0.89
                return new Rect(0.0f, 0.5f, 0.5f / ra, 0.5f);
            case 6:
                return new Rect(1f - 0.5f / ra, 0.0f, 0.5f / ra, 0.5f);
            default:
                return new Rect(0.0f, 0.5f, 0.5f, 0.5f);
        }
    }

    public void RemoveCamera(int i)
    {
        m_cameras[i].hasPlayer = false;
        m_cameras[i].m_Camera.gameObject.SetActive(false);
        RefreshCams();
    }

    public Boolean IsDead(int i)
    {
        if (m_cameras != null)
        {
            if (m_cameras.ContainsKey(i))
                return !m_cameras[i].hasPlayer;
            else return true;
        }
        else
        {
            return true;
        }
    }

    private int ActiveCams()
    {
        int i = 0;
        foreach(KeyValuePair<int, DisplaySection> cam in m_cameras)
        {
            if (cam.Value.hasPlayer) i++;
        }

        return i;
    }

    private void SetCameraTargets()
    {
        // Create a collection of transforms the same size as the number of tanks.
        Transform[] targets = new Transform[maxTanks];

        // For each of these transforms...
        for (int i = 0; i < targets.Length; i++)
        {
            // ... set it to the appropriate tank transform.
            if(m_Tanks[i].m_Instance!=null)
                targets[i] = m_Tanks[i].m_Instance.transform;
        }

        // These are the targets the camera should follow.
        m_CameraControl.m_Targets = targets;
    }


    // This is called from start and will run each phase of the game one after another.
    private IEnumerator GameLoop ()
    {
        // Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
        yield return StartCoroutine (RoundStarting ());

        // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
        yield return StartCoroutine (RoundPlaying());

        // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished.
        yield return StartCoroutine (RoundEnding());

        // This code is not run until 'RoundEnding' has finished.  At which point, check if a game winner has been found.
        if (m_GameWinner != null)
        {
            // If there is a game winner, restart the level.
            SceneManager.LoadScene (0);
        }
        else
        {
            // If there isn't a winner yet, restart this coroutine so the loop continues.
            // Note that this coroutine doesn't yield.  This means that the current version of the GameLoop will end.
            StartCoroutine (GameLoop ());
        }
    }

    private IEnumerator ChangePlayerView()
    {
        yield return StartCoroutine(ChangePlayerViewImpl());

        if (ActiveCams() == 3)
        {
            StartCoroutine(ChangePlayerView());
        }
    }

    private IEnumerator ChangePlayerViewImpl()
    {
        if (currentFollowCam >= m_cameras.Count) currentFollowCam = 0;
        int initialCurrentFollowCam = currentFollowCam;
        Boolean finish = false;
        while (!m_cameras.ContainsKey(currentFollowCam))
        {
            currentFollowCam++;
            if (currentFollowCam >= maxTanks) currentFollowCam = 0;
        }
        while (!m_cameras[currentFollowCam].hasPlayer && !finish)
        {
            currentFollowCam++;
            if (currentFollowCam >= maxTanks) currentFollowCam = 0;
            finish = initialCurrentFollowCam == currentFollowCam;
            while (!m_cameras.ContainsKey(currentFollowCam))
            {
                currentFollowCam++;
                if (currentFollowCam >= maxTanks) currentFollowCam = 0;
            }
        }

        if (!finish)
        {
            /*followCam.GetComponentInChildren<CinemachineVirtualCamera>().Follow =
                m_Tanks[m_cameras[currentFollowCam].position].m_Instance.transform;
            followCam.GetComponentInChildren<CinemachineVirtualCamera>().LookAt =
                m_Tanks[m_cameras[currentFollowCam].position].m_Instance.transform;*/
        }

        currentFollowCam++;
        yield return m_ChangePlayerWait;
    }

    private IEnumerator RoundStarting ()
    {
        // As soon as the round starts reset the tanks and make sure they can't move.
        ResetAllTanks ();
        DisableTankControl ();
        ResetAllCams();
        SetCameraTargets();
        RefreshCams();
        DisableMiniMap();

        // Snap the camera's zoom and position to something appropriate for the reset tanks.
        m_CameraControl.SetStartPositionAndSize ();

        // Increment the round number and display text showing the players what round it is.
        m_RoundNumber++;
        m_MessageText.text = "ROUND " + m_RoundNumber;

        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_StartWait;
    }


    private IEnumerator RoundPlaying ()
    {
        // As soon as the round begins playing let the players control the tanks.
        EnableTankControl ();
        EnableMiniMap();
        miniMap.GetComponent<MiniMapControlOffline>().SetPlayers();
        RefreshCams();

        // Clear the text from the screen.
        m_MessageText.text = string.Empty;

        // While there is not one tank left...
        while (!OneTankLeft())
        {
            // ... return on the next frame.
            yield return null;
        }
    }


    private IEnumerator RoundEnding ()
    {
        // Stop tanks from moving.
        DisableTankControl ();

        DisableMiniMap();

        // Clear the winner from the previous round.
        m_RoundWinner = null;

        // See if there is a winner now the round is over.
        m_RoundWinner = GetRoundWinner ();

        // If there is a winner, increment their score.
        if (m_RoundWinner != null)
            m_RoundWinner.m_Wins++;

        // Now the winner's score has been incremented, see if someone has one the game.
        m_GameWinner = GetGameWinner ();

        // Get a message based on the scores and whether or not there is a game winner and display it.
        string message = EndMessage ();
        m_MessageText.text = message;

        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_EndWait;
    }


    // This is used to check if there is one or fewer tanks remaining and thus the round should end.
    private bool OneTankLeft()
    {
        // Start the count of tanks left at zero.
        int numTanksLeft = 0;

        // Go through all the tanks...
        for (int i = 0; i < maxTanks; i++)
        {
            // ... and if they are active, increment the counter.
            //if (m_Tanks[i].m_Instance != null && m_Tanks[i].m_Instance.activeSelf)
            if (m_Tanks[i].m_Instance.activeSelf)
                numTanksLeft++;
        }

        // If there are one or fewer tanks remaining return true, otherwise return false.
        return numTanksLeft <= 1;
    }
        
        
    // This function is to find out if there is a winner of the round.
    // This function is called with the assumption that 1 or fewer tanks are currently active.
    private TankManagerOffline GetRoundWinner()
    {
        // Go through all the tanks...
        for (int i = 0; i < maxTanks; i++)
        {
            // ... and if one of them is active, it is the winner so return it.
            if (m_Tanks[i].m_Instance!=null && m_Tanks[i].m_Instance.activeSelf)
                return m_Tanks[i];
        }

        // If none of the tanks are active it is a draw so return null.
        return null;
    }


    // This function is to find out if there is a winner of the game.
    private TankManagerOffline GetGameWinner()
    {
        // Go through all the tanks...
        for (int i = 0; i < maxTanks; i++)
        {
            // ... and if one of them has enough rounds to win the game, return it.
            if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
                return m_Tanks[i];
        }

        // If no tanks have enough rounds to win, return null.
        return null;
    }


    // Returns a string message to display at the end of each round.
    private string EndMessage()
    {
        // By default when a round ends there are no winners so the default end message is a draw.
        string message = "DRAW!";

        // If there is a winner then change the message to reflect that.
        if (m_RoundWinner != null)
            message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";

        // Add some line breaks after the initial message.
        message += "\n\n\n\n";

        // Go through all the tanks and add each of their scores to the message.
        for (int i = 0; i < m_Tanks.Count; i++)
        {
            if(m_Tanks[i].m_Instance!=null)
                message += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
        }

        // If there is a game winner, change the entire message to reflect that.
        if (m_GameWinner != null)
            message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";

        return message;
    }


    // This function is used to turn all the tanks back on and reset their positions and properties.
    private void ResetAllTanks()
    {
        for (int i = 0; i < maxTanks; i++)
        {
            m_Tanks[i].Reset();
        }
    }

    private void ResetAllCams()
    {
        for (int i = 0; i < maxTanks; i++)
        {
            if (m_cameras.ContainsKey(i))
            {
                m_cameras[i].hasPlayer = true;
                m_cameras[i].m_Camera.gameObject.SetActive(true);
            }
        }

        GameObject.Find("CameraRig").GetComponent<CameraControlOffline>().SetCameras(true);

    }

    private void EnableTankControl()
    {
        for (int i = 0; i < maxTanks; i++)
        {
            m_Tanks[i].EnableControl();
        }
    }


    private void DisableTankControl()
    {
        for (int i = 0; i < maxTanks; i++)
        {
            m_Tanks[i].DisableControl();
        }
    }

    private void DisableMiniMap()
    {
        miniMap.SetActive(false);
    }

    private void EnableMiniMap()
    {
        miniMap.SetActive(true);
    }
}
