﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class UdpReceiver : MonoBehaviour {

    private int port = 11000;

    void Start()
    {
        bool done = false;
        UdpClient listener = new UdpClient(port);
        IPEndPoint ep = new IPEndPoint(IPAddress.Any, port);

        try
        {
            while (!done)
            {
                Debug.Log("Waiting for broadcast");
                byte[] b = listener.Receive(ref ep);
                Debug.Log("Broadcast from {0} :\n {1}\n");
                ep.ToString();
                Encoding.ASCII.GetString(b, 0, b.Length);
            }
        }catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
        finally
        {
            listener.Close();
        }
    }
}
