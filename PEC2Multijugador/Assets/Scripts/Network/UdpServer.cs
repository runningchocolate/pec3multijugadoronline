﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;
using UnityEngine;

public class UdpServer : MonoBehaviour {

    private string ip = "192.168.1.255";
    private int port = 11000;
    public string message = "hello";

    void Start()
    {
        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        IPAddress broadcast = IPAddress.Parse(ip);
        byte[] b = Encoding.ASCII.GetBytes(message);

        IPEndPoint ep = new IPEndPoint(broadcast, port);
        s.SendTo(b, ep);
        Debug.Log("Message sent to the broadcast address");
    }
}
