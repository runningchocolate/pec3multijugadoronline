﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class TcpTimeServer : MonoBehaviour {

    private int port = 11000;

    // Use this for initialization
    void Start () {

        //SERVER

        bool done = false;
        IPAddress localAddr = IPAddress.Parse("192.168.1.131");

        //TcpListener listener = new TcpListener(port);
        TcpListener listener = new TcpListener(localAddr, port);
        listener.Start();
        while (!done)
        {
            Debug.Log("Waiting for connection...");
            TcpClient client = listener.AcceptTcpClient();
            Debug.Log("Connection accepted");
            NetworkStream ns = client.GetStream();
            byte[] byteTime = Encoding.ASCII.GetBytes("Se me escucha o no?");
            try
            {
                ns.Write(byteTime, 0, byteTime.Length);
                ns.Close();
                client.Close();
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }
        listener.Stop();        
	}
}
