﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter2D : MonoBehaviour {

    public Text fpsLowHigh = null;
    public Text fpsActual = null;
    public Text frameBufferDisplay = null; 

    private Vector3[] fpsRates;
    private Vector3[] bufferRates;
    private int displayColumns = 100;

    private float maxWidth = 14f;
    private float columnDistance;

    private int lowFPS = 1000;
    private int highFPS = 0;

    private List<float> fps_lists;
    private int list_fps_num = 30;
    private int iFpsList;


    void Start()
    {
        bool missingReference = (fpsLowHigh == null || frameBufferDisplay==null);
      
        if(missingReference || Config.ACTUAL_MODE == Config.MODE.Release)
        {
            this.enabled = false;
            this.gameObject.SetActive(false);
        }
        
        else
        {
            DontDestroyOnLoad(transform.gameObject);    
            ResetFPSCounter();
        }

        fps_lists = new List<float>();
    }

    void Update()
    {
        float fps = 1f / Time.deltaTime;                                     
        bufferRates = fpsRates;

        for (int count = 0; count < fpsRates.Length-1; ++count)          
        {
            bufferRates[count].y = fpsRates[count + 1].y;
        }

        bufferRates[fpsRates.Length - 1].y =  ( (4f/60f)* fps) -3;
            
        fpsRates = bufferRates;

        if (fps_lists.Count < list_fps_num) fps_lists.Add(fps);
        else fps_lists[iFpsList] = fps;

        iFpsList++;
        if (iFpsList >= list_fps_num) iFpsList = 0;
        fpsActual.text = FpsAverageAndRecalculeLowHigh() + " fps";
        fpsLowHigh.text = lowFPS + " / " + highFPS;
    }

    private int FpsAverageAndRecalculeLowHigh()
    {
        float avg = 0;
        float min = -1;
        float max = -1;

        foreach (var fps in fps_lists)
        {
            avg += fps;
            if (min == -1) min = fps;
            else if (min > fps) min = fps;

            if (max == -1) max = fps;
            else if (max < fps) max = fps;
        }

        lowFPS = (int) min;
        highFPS = (int) max;

        return (int)(avg / fps_lists.Count);
    }

    void ResetFPSCounter()
    {
        columnDistance = maxWidth / displayColumns;
        fpsRates = new Vector3[displayColumns];

        for (int count = 0; count < fpsRates.Length; ++count)
        {
            fpsRates[count].Set(-(maxWidth * 0.5f) + (count * columnDistance), -3, 0.5f);

        }

        bufferRates = new Vector3[displayColumns];
        frameBufferDisplay.text = displayColumns.ToString();
    }
}