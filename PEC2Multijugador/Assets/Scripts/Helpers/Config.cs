﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config
{
    public enum MODE {
        Development, Release, DevelopmentAutomatic
    };

    public static MODE ACTUAL_MODE = MODE.Release;
}
