using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class TankController : NetworkBehaviour
{
    public static int currentPlayers = 0;
    public int currentPlayer;

    [SyncVar]
    public string nickname;

    [SyncVar]
    public Color colour;

    //bullet system
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public GameObject bulletCanvas;
    public const int maxBullets = 5;
    public int reloadTime = 2; //seconds
    private float startReloadTime = -1;
    private GameObject reloadBar;

    [SyncVar(hook = "OnChangeBullets")]
    public int currentBullets = maxBullets;


    //statistics params
    public GameObject statisticsPrefab;
    private float lastSyncT = -1f;
    private bool automaticMovement = false;
    private bool showStatistics = false;
    private float statisticsX = 0;
    private float statisticsHeight = -25f;
    private GameObject statistics;
    private List<float> statisticsListValues;
    private int currentStatistic = 0;
    private int statisticsNumber = 30;

    void Start()
    {
        currentBullets = maxBullets;
        reloadBar = GameObject.Find("ReloadBar");
        //reloadBar.SetActive(false);
        automaticMovement = Config.ACTUAL_MODE==Config.MODE.DevelopmentAutomatic;
        showStatistics = Config.ACTUAL_MODE == Config.MODE.Development || Config.ACTUAL_MODE == Config.MODE.DevelopmentAutomatic;
        currentPlayer = ++TankController.currentPlayers;

        nickname = "Player" + currentPlayers.ToString();
        CmdChangeName(nickname);

        if (showStatistics)
        {
            statisticsListValues = new List<float>();
            GameObject parent = GameObject.Find("PlayersStatistics");
            statistics = (GameObject)Instantiate(statisticsPrefab, parent.transform);
            statistics.transform.localPosition = new Vector3(statisticsX, statisticsHeight * currentPlayer, 0);
            statistics.transform.Find("PlayerText").GetComponent<Text>().text = "Player " + currentPlayer;
        }
    }

    void OnGUI()
    {
        if (isLocalPlayer)
        {
            nickname = GUI.TextField(new Rect(25, Screen.height - 40, 100, 30), nickname);
            if (GUI.Button(new Rect(130, Screen.height - 40, 80, 30), "Change"))
            {
                CmdChangeName(nickname);
            }

            if (GUI.Button(new Rect(250, Screen.height - 40, 80, 30), "Blue"))
            {
                CmdChangeColor(Color.blue);
            }
            
            if (GUI.Button(new Rect(350, Screen.height - 40, 80, 30), "Yellow"))
            {
                CmdChangeColor(Color.yellow);
            }

            if (GUI.Button(new Rect(450, Screen.height - 40, 80, 30), "Green"))
            {
                CmdChangeColor(Color.green);
            }

            if (GUI.Button(new Rect(550, Screen.height - 40, 80, 30), "Magenta"))
            {
                CmdChangeColor(Color.magenta);
            }

            if (GUI.Button(new Rect(650, Screen.height - 40, 80, 30), "Black"))
            {
                CmdChangeColor(Color.black);
            }

            if (GUI.Button(new Rect(750, Screen.height - 40, 80, 30), "Grey"))
            {
                CmdChangeColor(Color.grey);
            }

            if (GUI.Button(new Rect(850, Screen.height - 40, 80, 30), "White"))
            {
                CmdChangeColor(Color.white);
            }
        }
    }


    private void UpdateNicknameColor()
    {
        this.GetComponentInChildren<TextMesh>().text = nickname;

        foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>())
        {
            child.material.color = colour;
        }
    }

    private void UpdateStatistics()
    {
        if (showStatistics)
        {
            NetworkTransform nt = GetComponent<NetworkTransform>();
            //Debug.Log(nt.lastSyncTime);
            if (lastSyncT == -1)
            {
                lastSyncT = nt.lastSyncTime;
            }
            else
            {
                float elapsedTime = nt.lastSyncTime - lastSyncT;
                if (elapsedTime > 0)
                {
                    float value = 1f / elapsedTime;
                    statistics.transform.Find("FPSactual").GetComponent<Text>().text =
                        "(curr) " + ((int)value) + "FPS";
                    lastSyncT = nt.lastSyncTime;
                    if (statisticsNumber > statisticsListValues.Count) statisticsListValues.Add(value);
                    else statisticsListValues[currentStatistic] = value;
                    currentStatistic++;

                    if (currentStatistic >= statisticsNumber) currentStatistic = 0;
                    UpdateAvgMinMaxValues();
                }
            }
        }
    }

    private void UpdateMovement()
    {
        float x = 0;
        float z = 0;

        if (automaticMovement)
        {
            x = 0.5f * Time.deltaTime * 150.0f;
            z = 1f * Time.deltaTime * 3.0f;
        }
        else
        {
            x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
            z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
        }


        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
    }

    private void UpdateShootReload()
    {
        if (Input.GetKeyDown(KeyCode.Space) && currentBullets > 0 && startReloadTime == -1)
        {
            CmdFire();
        }
        else if (Input.GetKeyDown(KeyCode.Space) && currentBullets <= 0 && startReloadTime == -1)
        {
            Reload();
        }
        else if (Input.GetKeyDown(KeyCode.R) && startReloadTime == -1 && currentBullets < maxBullets)
        {
            Reload();
        }

        if (startReloadTime != -1)
        {
            float elapsedTime = Time.time - startReloadTime;
            if (elapsedTime < reloadTime)
            {
                float percentage = elapsedTime / ((float)reloadTime) * 100;
                GameObject fg = reloadBar.transform.Find("Foreground").gameObject;
                fg.GetComponent<RectTransform>().sizeDelta = new Vector2(percentage, 10f);
            }
            else
            {
                //reloadBar.SetActive(false);
                CmdReloadBullets();
                startReloadTime = -1;
                reloadBar.transform.Find("Foreground").GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 10f);
            }
        }
    }


    void Update() {
        UpdateNicknameColor();
        UpdateStatistics();

        if (!isLocalPlayer)
        {
            return;
        }

        UpdateMovement();
        UpdateShootReload();
    }


    //Commands


    // This [Command] code is called on the Client …
    // … but it is run on the Server!
    [Command]
    void CmdFire()
    {       
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletSpawn.position,
            bulletSpawn.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 10;

        // Spawn the bullet on the Clients
        NetworkServer.Spawn(bullet);

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
        currentBullets--;
    }

    [Command]
    public void CmdChangeName(string name)
    {
        nickname = name;
    }

    [Command]
    public void CmdChangeColor(Color color)
    {
        colour = color;
    }

    [Command]
    void CmdReloadBullets()
    {
        currentBullets = maxBullets;
    }

    private void Reload()
    {
        if (isLocalPlayer)
        {
            startReloadTime = Time.time;
            //reloadBar.SetActive(true);
            reloadBar.transform.Find("Foreground").GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 10f);
        }
    }

    private void OnChangeBullets(int currentBullet)
    {
        //Debug.Log("Current bullet: " + currentBullet + " " + gameObject.name);
        int i = 0;
        currentBullets = currentBullet;
        Transform[] bulletList = bulletCanvas.transform.GetComponentsInChildren<Transform>();
        foreach (Transform bullet in bulletList)
        {
            if (bullet.gameObject.GetComponent<RawImage>() != null)
            {
                if (i >= currentBullet)
                {
                    RawImage rawImage = bullet.gameObject.GetComponent<RawImage>();
                    Color currColor = rawImage.color;
                    currColor.a = 0.5f;
                    rawImage.color = currColor;
                }
                else
                {
                    RawImage rawImage = bullet.gameObject.GetComponent<RawImage>();
                    Color currColor = rawImage.color;
                    currColor.a = 1f; rawImage.color = currColor;
                    rawImage.color = currColor;
                }
                i++;
            }            
        }
    }
 
    private void UpdateAvgMinMaxValues()
    {
        float result = 0;
        float min = -1;
        float max = -1;

        foreach (float v in statisticsListValues)
        {
            result += v;

            if (min == -1) min = v;
            else if (min > v) min = v;

            if (max == -1) max = v;
            else if (max < v) max = v;
        }
        statistics.transform.Find("FPSavg").GetComponent<Text>().text = "(avg) " + (int)(result / statisticsListValues.Count) + "FPS";
        statistics.transform.Find("FPSminMax").GetComponent<Text>().text = "(min) " + (int)min + "FPS - (max) " + (int)max + "FPS";
    }


    public override void OnStartLocalPlayer() {

        foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>())
        {
            child.material.color = Color.blue;
        }

        colour = Color.blue;      
        CmdChangeColor(Color.blue);
    }
}
