# PEC3MultijugadorOnline

Esta actividad consiste en consolidar el proyecto que hemos ido trabajando a lo largo de la asignatura para obtener un juego más completo y publicable : Tanks! UOC.

Siguiendo los pasos de las actividades anteriores, está pensada para realizarse en grupos de 3-4 alumnos

Dado que iniciaremos el primer requerimiento es permitir tanto juego online como juego offline, la recomendación para este tipo de juegos es realizar en última instancia la parte offline a partir de la online. Para empezar la idea es partir los dos recursos más relacionados que hemos trabajado en la asignatura :

Solución PEC2 (online)
Solución PEC1 (offline)
Otra característica que se pide es el soporte de partida por equipos. Hasta ahora hemos trabajado la modalidad “todos contra todos”, pero en este nuevo proyecto se debe modificar el juego para dar la opción de escoger compañero y realizar partidas 2 vs 2. En este caso la puntuación será compartida (similar a un 1 vs 1), los tanques tendrán el mismo color en cada equipo (azul vs rojo) y una vez finalizada la partida al volver a la pantalla de selección se podrá cambiar de equipo.
 
Es por ello que en la pantalla de selección de partida deben haber como mínimo las siguientes opciones : 

Juego Offline (Local)
1 vs 1

Juego Online (LAN)
2 vs 2

Juego Online (WAN)

Todos contra todos

Otro aspecto a trabajar en el juego online, es habilitar  las partidas por Internet (WAN) siendo necesario informar de la IP del Host a todos los participantes para que puedan unirse al Lobby, a diferencia del mecanismo de búsqueda por LAN que teníamos hasta ahora. Para simplificar, la solución básica se puede realizar fuera del juego (ej. chat externo), aunque hay lugar a la mejora implementando soluciones más profesionales.

Para el apartado de pruebas, en esta entrega se requiere realizar una serie de videos donde se incluyan los diferentes modos, en especial y que requiere grabación simultanea, las pruebas del juego online entre los integrantes del grupo.

Otro requisito de esta actividad es crear un proyecto en Unity Connect en el cual publiquéis vuestro producto final. Este paso es un requisito para la evaluación del ejercicio y la calidad del showcase que creeis se valorará de cara a la nota final

Por último, se valorará  las opcionalidades (ej. minimapa) incluidas en la entrega final y la gestión de tareas realizada en Trello.

Por lo tanto, las tareas a realizar son : 

Permitir juego multijugador online y offline (local)
Pantalla de selección de modos de juego
Modificar el juego para dar soporte a equipos
Mostrar nicknames y color de equipo
Habilitar partidas en WAN (IP del Host)
Realizar y grabar pruebas de hasta 4 jugadores
Crear el espacio completo en Unity Connect
Gestionar las tareas en Trello
Otras tareas opcionales podrían ser : 

Permitir configurar el fuego amigo
Añadir un minimapa en el modo online (pantalla completa)
Añadir NPCs (ej. trampas o torretas)
Añadir diferentes armas o tipos de balas
Implementar solución matchmaker (ej. Unity Multiplayer)
 

Resultado y entrega

Siguiendo las mismas pautas que en las actividades anteriores, se debe entregar el asset exportado y una documentación de los puntos importantes en formato PDF. 







==============================================





Hola a todos,

Empezamos el último módulo donde trabajaremos un tema importante para cualquier videojuego, no sólo para los multijugador, como son los servicios online.

Para ello iniciamos con el propio concepto y las plataformas que lo explotan en la actualidad, centrándonos en la más conocida, qué es Steam. Estos servicios suelen ser comunes en su mayoría, como son los logros, marcadores, estadísticas o savegames, o como especializados para los juegos multijugador, como son el lobby, matchmaking, networking o servicios anti-trampa.

A continuación, repasaremos el concepto de Cloud y los mecanismos que permiten interconectar los clientes (en nuestro caso los juegos en Unity) con las plataformas en la nube y los servicios que estos proporcionan. Para esta ilustrar el funcionamiento de una plataforma real en este módulo veremos cómo funciona un sistema de actores remotos. Para entender el concepto de los actores os recomiendo ver el siguiente vídeo del proyecto Orleans, de Microsoft : 

https://www.youtube.com/watch?v=I91ZU8tEJkU&t=776s

En nuestro caso usaremos la versión Java (proyecto Orbit) que está fuertemente basada en Orleans, pero que fue desarrollada por Bioware (Electronics Arts) y que se empezó a usar en Dragon Age Inquisition, así como en otros juegos de EA más recientes como Mass Effect Andromeda : 

Finalmente, llegamos al último proyecto de esta asignatura, siendo ?Tanks! Chat? un ejemplo de como conectar el ejemplo visto a lo largo de estos módulos a un servicio en concreto en la nube, como es el chat. Servicio simple en su concepción, pero que implica una bidireccionalidad muy interesante para ser aplicada en otros casos más complejos.

Este proyecto (disponible la base en este post), hay que trabajarlo en dos partes :

1) Servidor

A diferencia del resto del curso, en este caso se requiere una forma de trabajar diferente, ya que se trata de un proyecto Java (Orbit) y se requiere la instalación del entorno.

Java (JDK 8) : https://www.java.com/es/download/
Maven : https://maven.apache.org/download.cgi

En este caso, tenéis un ejemplo del proyecto oficial (https://github.com/orbit/orbit-samples/tree/master/chat). Para compilar el proyecto y ejecutarlo, os recomiendo arrancar el backend (actores) y el frontal (web) para siguiendo estos pasos (en sistemas Linux/Mac) : 

Compilamos el proyecto : chat> mvn compile
Damos permisos de ejecución : chat> chmod +x *.sh
Arrancamos el back-end : chat> ./start-backend.sh
Arrancamos el front-end : chat> ./start-frontend.sh

Finalmente, revisamos que el sistema funciona bien desde el navegador : http://localhost:8080

2) Cliente

Esta parte os será tan simple como siempre al tratarse de un asset de proyecto Unity que gracias a un cliente de WebSockets es capaz de comunicarse con el servidor para consumir/producir mensajes de Chat

Dado que el uso de estas librerías WebSockets en el servidor puede fallar dependiendo del SO / Navegador utilizado, os recomiendo testear vuestro entorno mediante herramientas como : https://websocketstest.com

Empezamos aquí y como de costumbre, en los próximos días os iré indicando los recursos a utilizar y emplear en este módulo.

Saludos,

------------------------------------------------------------------------------------------------------------------------------------------------------------
Como apéndice, ps dejo aquí información ampliada de cómo funciona el tema WebSockets en el ejemplo que estamos usando en este módulo para que podáis entender mejor cómo está implementado:

1) Ejemplo WebSockets con Java : 
https://blog.openshift.com/how-to-build-java-websocket-applications-using-the-jsr-356-api/

2) Implementación Servidor Web Orbit (Jetty) : 
https://github.com/orbit/orbit-samples/blob/master/chat/chat-frontend/src/main/java/cloud/orbit/samples/chat/ChatWebSocket.java

3) Implementación Cliente Web Orbit (Html/JavaScript) : 
https://github.com/orbit/orbit-samples/blob/master/chat/chat-frontend/src/main/resources/web/index.html

4) De cara al uso del servidor con el cliente Unity, podéis ver que este incluye una biblioteca de websockets para C# que es muy similar y conceptualmente idéntica : 
https://github.com/sta/websocket-sharp







===============================================



Buenas a todos,


Como ya sabéis el desarrollo de un videojuego puede ir desde una persona a estudios de centenares de trabajadores.


En esta asignatura además se suma el hecho que estamos desarrollando juegos multijugador siendo necesario un número de jugadores (dos como mínimo).

Para los estudios indie encontrar los perfiles necesarios para desarrollar el proyecto planteado supone un reto, siendo buena idea buscar en plataformas creadas para este fin (ej. Unity Connect).

Una vez se establece el equipo se requiere ser ágiles y poder trabajar de forma asíncrona para que todos los componentes del equipo puedan avanzar a su ritmo. No entraremos en las metodologías ágiles (ej. Scrum o Kanban) pero sí usaremos uno de sus elementos principales : el TaskBoard (ej. Trello).

Mediante la herramienta online Trello, podemos trabajar en equipo y realizar el seguimeinto del proyecto  : https://www.youtube.com/watch?v=tOpBJnOifAc

Recomendaciones :

Utilizar nombres de tareas cortos (3-4 palabras)
Describir con más detalle el objetivo de la tarea en el campo "Description"
Añadir imágenes siempre que aporte más valor que una descripción
Utilizar checklist para detallar las subtareas a realizar
Si una checklist es muy larga, señal de que la tarea debe ser particionada
No añadir una subtarea que tenga entidad para ser tarea de por sí
Marcar con deadlines las fechas de finalización estimada de cada tarea
Mantener el tablero actualizado con alta frecuencia
Revisar que la URL del tablero sea accesible externamente
Por otro lado, para poder compartir nuestro proyecto con el resto de compañeros, hay dos alternativas recomendadas :

Utilizar un repostorio Git, siendo el estandard en este tipo de sistemas de colaboración y podéis utilizar por ejemplo las herramientas de Atlassian (SourceTree y BitBucket) siguiendo el tutorial oficial de Unity : https://unity3d.com/es/learn/tutorials/topics/cloud-build/creating-your-first-source-control-repository
Otra opción sería utilizar el propio sistema de Unity, aunque está más limitado, por ejemplo a tres usuarios, en su versión gratuita : https://unity3d.com/es/unity/features/collaborate
Como os he comentado, esta PEC2 es una gran oportunidad para empezar a explorar estos aspectos del trabajo en equipo que os serán de gran utilizad para superar la competencia de la asignatura y este aspecto clave para entrar en el mundo profesional, siendo un requisito de la industria del software en general.





==========================================


