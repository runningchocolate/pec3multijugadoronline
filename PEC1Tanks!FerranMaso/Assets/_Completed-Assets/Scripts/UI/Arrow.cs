﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Complete
{
    public class Arrow
    {
        public Vector2 position;
        public Vector3 rotation;
        public GameObject arrow;
        public Color color;
        public int player;
    }
}
