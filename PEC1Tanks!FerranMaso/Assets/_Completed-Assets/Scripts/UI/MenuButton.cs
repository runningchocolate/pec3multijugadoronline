﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Complete
{
    public class MenuButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {

        public float scaleSize = 1.22f;
        public float defaultScaleSize = 1f;

        void Start()
        {
            GetComponent<Transform>().localScale = new Vector3(defaultScaleSize, defaultScaleSize, defaultScaleSize);
        }

        void Update()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                GetComponent<Transform>().localScale = new Vector3(defaultScaleSize, defaultScaleSize, defaultScaleSize);
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            GetComponent<Transform>().localScale = new Vector3(scaleSize, scaleSize, defaultScaleSize);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            GetComponent<Transform>().localScale = new Vector3(defaultScaleSize, defaultScaleSize, defaultScaleSize);
        }
    }
}