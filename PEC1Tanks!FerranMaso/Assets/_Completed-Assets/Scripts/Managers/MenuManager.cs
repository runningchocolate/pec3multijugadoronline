﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Complete
{
    public class MenuManager : MonoBehaviour
    {

        public TankManager[] m_Tanks;
        public int currentTanks = 0;
        public int minTanks = 2;
        public GameObject m_TankPrefab;

        public GameObject minusButton;
        public GameObject plusButton;

        public Text playersText;

        private String playersDefaultText = "Jugadores: ";

        // Use this for initialization
        void Start()
        {
            currentTanks = 0;
            for (int i = 0; i < GameManager.theoricTankNumber; i++) CreateTank();
            playersText.text = playersDefaultText + currentTanks;
            if (currentTanks >= m_Tanks.Length) plusButton.SetActive(false);
            if (currentTanks <= minTanks) minusButton.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void AddTank()
        {
            CreateTank();
            GameManager.theoricTankNumber = currentTanks;
        }

        public void CreateTank()
        {
            if (currentTanks < m_Tanks.Length)
            {
                m_Tanks[currentTanks].m_Instance =
                    Instantiate(m_TankPrefab, m_Tanks[currentTanks].m_SpawnPoint.position,
                        m_Tanks[currentTanks].m_SpawnPoint.rotation) as GameObject;
                m_Tanks[currentTanks].m_PlayerNumber = currentTanks + 1;
                m_Tanks[currentTanks].Setup();
                currentTanks++;
                playersText.text = playersDefaultText + currentTanks;
                if (currentTanks >= m_Tanks.Length) plusButton.SetActive(false);
                if (currentTanks > minTanks) minusButton.SetActive(true);
            }
        }

        public void DestroyTank()
        {
            if (currentTanks > 0)
            {

                m_Tanks[currentTanks - 1].m_Instance.SetActive(false);
                currentTanks--;
                GameManager.theoricTankNumber = currentTanks;
                playersText.text = playersDefaultText + currentTanks;
                if (currentTanks < m_Tanks.Length) plusButton.SetActive(true);
                if (currentTanks <= minTanks) minusButton.SetActive(false);
            }
        }
    }
}