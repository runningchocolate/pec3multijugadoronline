﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Complete
{
    public class DisplaySection
    {

        public Camera m_Camera;
        public int position;
        public Boolean hasPlayer;

        public DisplaySection(Camera c, int p, Boolean hp)
        {
            m_Camera = c;
            position = p;
            hasPlayer = hp;
        }
    }
}